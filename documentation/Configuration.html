<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This manual is for SweetAda.

Copyright (C) 2020, 2021 Gabriele Galeotti
 -->
<!-- Created by GNU Texinfo 6.1, http://www.gnu.org/software/texinfo/ -->
<head>
<title>SweetAda: Configuration</title>

<meta name="description" content="SweetAda: Configuration">
<meta name="keywords" content="SweetAda: Configuration">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="makeinfo">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="index.html#Top" rel="start" title="Top">
<link href="Index.html#Index" rel="index" title="Index">
<link href="index.html#Top" rel="up" title="Top">
<link href="Makefile-targets.html#Makefile-targets" rel="next" title="Makefile targets">
<link href="Building-the-RTS.html#Building-the-RTS" rel="prev" title="Building the RTS">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>


</head>

<body lang="en">
<a name="Configuration"></a>
<div class="header">
<p>
Next: <a href="Makefile-targets.html#Makefile-targets" accesskey="n" rel="next">Makefile targets</a>, Previous: <a href="Building-the-RTS.html#Building-the-RTS" accesskey="p" rel="prev">Building the RTS</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Configuration-1"></a>
<h2 class="chapter">7 Configuration</h2>
<a name="index-configuration"></a>

<p>SweetAda configuration is managed by a central Makefile, which loads configuration files
in sequential steps.
</p>
<p>Configuration of SweetAda is done mainly with the help of two configuration files: &quot;main&quot;
(or &quot;system&quot;) configuration file, and a &quot;platform&quot; configuration file. The main configuration
file (located in the top-level directory) defines general, target-agnostic characteristics,
like stating Ada95 or Ada2012 mode, the optimization level, and so on; the platform configuration
file (located in the platform&rsquo;s directory) defines in greater detail how the target software
is going to be generated, and various parameters affecting low-level behaviour.
</p>
<p>Both files are, as a matter of fact, Makefile fragments that are going to be incorporated in
the build system at compile-time. Being the SweetAda build system based on GNU Make, many
facilitations are possible (like variable substitutions) and, if correctly arranged,
configuration items can be made modular and easily changeable.
</p>
<p>The final output product of SweetAda compilation is the file &quot;kernel.o&quot;. Its format depends
on the type of toolchain used, but it is almost exclusively an ELF-class object file.
</p>
<p>NOTE: The SweetAda build machinery is Makefile-based, but you can also use an alternate
GPR-style build model &ndash; this is enabled by a configuration variable.
</p>
<p>The master Makefile try to detect the type of machine it is running on, and configuration
of SweetAda begins by reading the system configuration file &quot;configuration.in&quot;.
</p>
<p>Essential variables are set to a default value, like the path to the toolchain prefix,
the build model, RTS type, profile, and so on.
</p>
<p>One important aspect is that configuration variables could be introduced (and could
override pre-defined ones) at various stages of the configuration. Obviously, further
and more specific configuration files are loaded in sequence from the very few basic
information found in the top-level configuration.in. 
</p>
<p>Then, if it exists, a &quot;kernel.cfg&quot; file from the top-level directory is loaded.
kernel.cfg contains the hardware platform which will be the build target. If kernel.cfg
is missing, you can specify the platform (and eventually the subplatform) by assigning
the shell environment variables PLATFORM and SUBPLATFORM, which in that case take
precedence over an existing file. PLATFORM should be a valid directory in the &quot;platforms&quot;
subdirectory, and SUBPLATFORM should be a valid subdirectory of the PLATFORM directory,
named &quot;platform-&lt;SUBPLATFORM&gt;&quot;.
</p>
<div class="example">
<pre class="example">$ PLATFORM=PC-x86 SUBPLATFORM=QEMU-ROM make createkernelcfg
</pre></div>

<p>NOTE: for a cmd.exe shell you have to use a &quot;SET&quot; command.
</p>
<p>Once you choose a platform, you can create the kernel.cfg with the Makefile auxiliary
target &quot;createkernelcfg&quot;.
</p>
<p>Following this command, you can check the presence of a correct kernel.cfg file.
The contents of kernel.cfg will be something like:
</p>
<div class="example">
<pre class="example">PLATFORM := PC-x86
SUBPLATFORM := QEMU-ROM
</pre></div>

<p>NOTE: You can create the kernel.cfg by hand and skip the createkernelcfg Makefile target.
</p>
<p>Once kernel.cfg does exist, it will be used until you issue a &quot;make all-clean&quot; command,
which erases every configuration file and objects from last compilation.
</p>
<p>NOTE: Issuing a new configuration erases the old one by executing an implicit &quot;all-clean&quot;
Makefile target.
</p>
<p>At this point, you should build the RTS.
See <a href="Building-the-RTS.html#Building-the-RTS">Building the RTS</a>
</p>
<p>After the top-level configuration file is loaded, and thus Makefile knows the platform,
the configuration.in found in that directory is loaded (e.g., ./platforms/ML605/configuration.in).
Here you can specify nearly everything about the target machine, like the CPU model,
fine-tuning compiler switches, run- or debug-related auxiliary scripts, and so on.
If you need to create some information that will be used by external scripts, this is right place.
Pay attention to export every variable you are creating that is not known by the
build system, otherwise the external scripts are not able to access it.
</p>
<div class="example">
<pre class="example">CPU       := x86
CPU_MODEL := i586

GCC_SWITCHES_PLATFORM     += -march=i586 -Wa,-march=i586
LD_SWITCHES_PLATFORM      +=
OBJDUMP_SWITCHES_PLATFORM +=
POSTBUILD_ROMFILE         := Y
OBJCOPY_SWITCHES_PLATFORM += --output-target=binary --gap-fill=0x00

USE_CLIBRARY := Y

USE_APPLICATION := test-PC-x86

ADDITIONAL_OBJECTS += ctest

SESSION_START_COMMAND :=
SESSION_END_COMMAND   :=

RUN_COMMAND   := runsweetada$(EXEEXT) -k &quot;$(SWEETADA_PATH)&quot; -p &quot;$(PLATFORM_DIRECTORY)&quot; -f qemu.cfg
DEBUG_COMMAND := $(RUN_COMMAND) -d DEBUG_QEMU

export WIRESHARK := /usr/bin/wireshark
</pre></div>

<p>NOTE: In the example above, we create and export the variable WIRESHARK, which will be used
by the launch script in order to do monitoring of network activity.
</p>
<p>Having knowledge of the hardware configuration, then the configuration.in in the
appropriate CPU hierarchy is loaded (e.g., cpus/MIPS/configuration.in).
</p>
<p>NOTE: Once the CPU is well defined, the right files can be taken into account. The standard
toolchain is selected by default, but you can pick another one by, e.g., overriding
the variable TOOLCHAIN_NAME_&lt;CPU&gt; in the configuration.in of the platform directory.
</p>
<p>Then, the top-level &quot;Makefile.tc.in&quot; file is taken into account. This a very
important part of the global configuration scenario, since it is responsible of the
setup parameters for the compiler toolchain, compilation switches, warning switches
and executable paths. If you want to suppress checks, warnings, or change programming
style layout, you can comment/uncomment the appropriate compiler switch.
</p>
<p>Now that the configuration setup is complete, the build system is ready for the next
step, the configuration target proper.
</p>
<div class="example">
<pre class="example">$ make configure
</pre></div>

<p>The &quot;configure&quot; Makefile target will call the homonym target in every sub-Makefile
concerned, e.g., &quot;core&quot;, &quot;modules&quot;, &quot;drivers&quot;, and the platform. This way, fixing of
files that should be included in the build or a pre-processing from a template file
can take place. As an example, files from SUBPLATFORM specific directory could be
symlinked in the platform set of files, the ?gnat.adc? file will be created from the
template file &quot;gnat.adc.in&quot;, and so on.
</p>
<p>The build system will reply by writing a brief configuration status:
</p>
<div class="example">
<pre class="example">Configuration parameters:
PLATFORM:           PC-x86
SUBPLATFORM:        QEMU-ROM
CPU:                x86
CPU MODEL:          i586
OSTYPE:             linux
SWEETADA PATH:      /root/project/sweetada
TOOLCHAIN PREFIX:   /opt/sweetada
TOOLCHAIN NAME:     i686-sweetada-elf
RTS ROOT PATH:      /root/project/sweetada/rts/sfp/i686-sweetada-elf
RTS PATH:           /root/project/sweetada/rts/sfp/i686-sweetada-elf/.
RTS:                sfp
PROFILE:            sfp
USE LIBADA:         Y
BUILD MODE:         MAKEFILE
OPTIMIZATION LEVEL: 1
MAKE:               make
GCC VERSION:        11.1.0
GCC SWITCHES:       -march=i586 -Wa,-march=i586
GCC MULTIDIR:       .
LD SCRIPT:          linker.lds
LD SWITCHES:        
OBJCOPY SWITCHES:   --output-target=binary --gap-fill=0x00
OBJDUMP SWITCHES:
</pre></div>

<p>The SweetAda Makefile build system can be queried. The special Makefile target
&quot;probevariable&quot; outputs the value of a configuration variable, so, e.g., you can execute a
command line like:
</p>
<div class="example">
<pre class="example">VERBOSE= PROBEVARIABLE=PROFILE make -s probevariable
</pre></div>

<p>and the build system will print the value associated with the variable PROFILE.
A script could intercept the output by means of a redirection and use this information.
Note that the VERBOSE variable is set empty and the the &quot;-s&quot; option is passed down
to the Makefile so the output is not cluttered with other non-interesting output.
</p>
<hr>
<div class="header">
<p>
Next: <a href="Makefile-targets.html#Makefile-targets" accesskey="n" rel="next">Makefile targets</a>, Previous: <a href="Building-the-RTS.html#Building-the-RTS" accesskey="p" rel="prev">Building the RTS</a>, Up: <a href="index.html#Top" accesskey="u" rel="up">Top</a> &nbsp; [<a href="Index.html#Index" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
